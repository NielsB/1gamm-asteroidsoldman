﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainManager : MonoBehaviour {

    public Camera mainCam;
    public GameObject starPrefab;
    public int starAmountPerScreen;
    public GameObject player;
    List<GameObject> starList;
    Random rand;
    float zOffset = 10;

	// Use this for initialization
	void Start () {

        starList = new List<GameObject>();

        for (int i = 0; i < starAmountPerScreen; i++)
        {
            GameObject clone = Instantiate(starPrefab, new Vector3(-100, -100, -10), Quaternion.identity) as GameObject;
            starList.Add(clone);
        }

        foreach(GameObject star in starList) {
            star.transform.position = randomPosWithinBounds();
        }

        Debug.Log("Rand pos 1: " + randomPosWithinBounds());
        Debug.Log("Rand pos 2: " + randomPosWithinBounds());
        Debug.Log("Rand pos 3: " + randomPosWithinBounds());
        Debug.Log("Rand pos 4: " + randomPosWithinBounds());
        Debug.Log("Rand pos 5: " + randomPosWithinBounds());

	}
	
	// Update is called once per frame
	void Update () {
	
        

	}

    private Vector3 randomPosWithinBounds()
    {
        Vector3 camPos = Camera.main.WorldToScreenPoint(mainCam.transform.position);

        Random.seed = (int)System.DateTime.Now.Ticks;
        float randX = Random.Range(camPos.x - (Screen.width /2), camPos.x + (Screen.width/2));
        float randY = Random.Range(camPos.y - (Screen.height / 2), camPos.y + (Screen.height / 2));

        

        Debug.Log("Rand X: " + randX);
        Debug.Log("Rand Y: " + randY);

        Vector3 randPos = Camera.main.ScreenToWorldPoint(new Vector3(randX, randY, zOffset)); 



        return randPos;


    }

}
