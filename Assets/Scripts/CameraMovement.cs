﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
        Vector3 playerPos = player.transform.position;

        transform.position = new Vector3(0, playerPos.y, -1);

	}
}
