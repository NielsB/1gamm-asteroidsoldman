﻿using UnityEngine;
using System.Collections;

[RequireComponent ((typeof (Rigidbody2D)))]
public class PlayerMovement : MonoBehaviour {

    public float movementSpeed;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(new Vector2(0, movementSpeed * Time.deltaTime));
        }
      
	}
}
