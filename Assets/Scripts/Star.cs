﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {

    Camera mainCam;
    float zOffset = 10;

	// Use this for initialization
	void Start () {

        mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

	}
	
	// Update is called once per frame
	void Update () {
        if (!withinBounds())
        {
            transform.position = randomPosWithinBounds();
        }
	}

    private bool withinBounds()
    {

        Vector3 camPos = Camera.main.WorldToScreenPoint(mainCam.transform.position);
        Vector2 topLeft = Camera.main.ScreenToWorldPoint(new Vector2(camPos.x - (Screen.width / 2), camPos.y - (Screen.height / 2)));
        Vector2 bottomRight = Camera.main.ScreenToWorldPoint(new Vector2(camPos.x + (Screen.width / 2), camPos.y + (Screen.height / 2)));

        Vector3 position = transform.position;

        if(position.x < topLeft.x || 
           position.x > bottomRight.x ||
           position.y < topLeft.y  ||
           position.y > bottomRight.y )
        {
            return false;
        }

        return true;
    }

    private Vector3 randomPosWithinBounds()
    {
        Vector3 camPos = Camera.main.WorldToScreenPoint(mainCam.transform.position);

        Random.seed = (int)System.DateTime.Now.Ticks;
        float randX = Random.Range(camPos.x - (Screen.width / 2), camPos.x + (Screen.width / 2));
        float randY = Random.Range(camPos.y - (Screen.height / 2), camPos.y + (Screen.height / 2));



        Debug.Log("Rand X: " + randX);
        Debug.Log("Rand Y: " + randY);

        Vector3 randPos = Camera.main.ScreenToWorldPoint(new Vector3(randX, randY, zOffset));



        return randPos;


    }
}
