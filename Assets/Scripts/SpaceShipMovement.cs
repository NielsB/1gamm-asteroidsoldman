﻿using UnityEngine;
using System.Collections;

public class SpaceShipMovement : MonoBehaviour {

    public float rotationSpeed;
	// Use this for initialization
	void Start () {   
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationSpeed);
            Debug.Log("Rotating left");
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.forward * -rotationSpeed);
            Debug.Log("Rotating right");
        }
	}
}
